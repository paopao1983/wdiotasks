/**
 * Automate the following script:


Open https://pastebin.com/ or a similar service in any browser.
Create 'New Paste' with the following attributes:
* Code: "Hello from WebDriver"
* Paste Expiration: "10 Minutes"
* Paste Name / Title: "helloweb"

*/
describe('WebdriverIO Module Automation Tasks', ()=>{

    beforeEach(async ()=>{
        await browser.url('https://pastebin.com/');
        browser.maximizeWindow();
    });

    it('task 1', async()=>{
      
        await $('a.header__btn').click();

        await $('textarea#postform-text').waitForEnabled();
        await $('textarea#postform-text').setValue('Hello from WebDriver');
    
        await $('div.form-group.field-postform-expiration').waitForClickable();
        await $('div.form-group.field-postform-expiration').click();
 
        await $('li=10 Minutes').waitForDisplayed();
        await $('li=10 Minutes').click();

        await $('input#postform-name').setValue('helloweb');

        //Save 'paste' and check the following:
        await $('vli#hideSlideBanner').waitForDisplayed();
        if(await $('vli#hideSlideBanner').isDisplayed)
        {
            await $('vli#hideSlideBanner').click();
        }
    
        await $('button.btn.-big').waitForEnabled();
        await $('button.btn.-big').click();

        const notification =  await $('div.notice.-success.-post-view');
        await notification.waitForDisplayed();
        await expect(notification).toBeDisplayed();
    });

    it('task 2', async()=>{
        await $('a.header__btn').click();
 
        const code = `git config --global user.name  "New Sheriff in Town"
        git reset $(git commit-tree HEAD^{tree} -m "Legacy code")
        git push origin master --force`;
        
        const textAtea = await $('textarea#postform-text');
        await textAtea.waitForEnabled();
        await textAtea.setValue(code);
    
        //* Syntax Highlighting: "Bash"
        const syntax_list = $('span#select2-postform-format-container');
        await syntax_list.waitForClickable();
        await syntax_list.click();      
        
        const syntax = await $('[type="search"]');
        await syntax.setValue('Bash');      
        await $('[role="option"]').click();  
 
        //* Paste Expiration: "10 Minutes"
        const listExpiration = await $('div.form-group.field-postform-expiration');
        await listExpiration.waitForClickable();
        await listExpiration.click();
 
        const expiration = await $('li=10 Minutes');
        await expiration.waitForDisplayed();
        await expiration.click();
 
        //* Paste Name / Title: "how to gain dominance among developers"
        const titleData = 'how to gain dominance among developers';
        const postform_name = await $('input#postform-name');
        await postform_name.setValue(titleData);
         
        //Save 'paste' and check the following:
        const ad = await $('vli#hideSlideBanner');
        if(await ad.isDisplayed())
            await ad.click();
 
        await $('button.btn.-big').waitForEnabled();
        await $('button.btn.-big').click();
        
        //* Browser page title matches 'Paste Name / Title'
        await expect(browser).toHaveTitle(titleData+' - Pastebin.com');
        
        //* Syntax is suspended for bash
        //no such suspended element 
        
        //* Check that the code matches the one from paragraph 2
        const codeSaved = await $('ol.bash');
        await codeSaved.waitForEnabled();
        await expect(codeSaved).toHaveText(code);
    });

});